#!/usr/bin/env python
#Copyright 2018 Sarath K

#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# license removed for brevity

import rospy
from std_msgs.msg import String
 					 
##Function for display the subscribed text from node speech_to_sentimental_analyzer
def callback(data):					
        rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data) 		
        rospy.loginfo(data.data)
# Three different lists that contain the possible words
        positive=['good','smile','happy']					
        negative=['angry','unhappy','sad']
        neutral=['ok','nice','hello']
#Checking the recieved word is positive or not
        for i in positive:					
	           if i==data.data:
                   s= "the word is positive"
# Defines interface of the node to the rest of the ros
                   pub=rospy.Publisher('audio',String,queue_size=10)	
                   rospy.init_node('listener', anonymous=True)			
                   rate=rospy.Rate(1) #1Hz
#If it is positive , then display it
                   while not rospy.is_shutdown():
                    	y=s
                   	rospy.loginfo(y)
                    	pub.publish(y)
                    	rate.sleep()
#Checking the recieved word is negative or not
        for i in negative:
                  if i==data.data:
                  	s= "the word is negative"
                  	pub=rospy.Publisher('audio',String,queue_size=10)
                  	rospy.init_node('listener', anonymous=True)
                  	rate=rospy.Rate(1)
#If it is negative , then display it
                  while not rospy.is_shutdown():
                    	y=s
                    	rospy.loginfo(y)
                    	pub.publish(y)
                    	rate.sleep()
#Checking the recieved word is neutral or not
        for i in neutral:
                   if i==data.data:
                   	s="the word is neutral"
                   	pub=rospy.Publisher('audio',String,queue_size=10)
                   	rospy.init_node('listener', anonymous=True)
                   	rate=rospy.Rate(1)
#If it is neutral , then display it
                   while not rospy.is_shutdown():
                     	y=s
                     	rospy.loginfo(y)
                     	pub.publish(y)
                     	rate.sleep()

# Function for subscribing data published from node speech_to_text 
def listener():
# In ROS, nodes are uniquely named. If two nodes with the same node are launched,
# the previous one is kicked off.
# The anonymous=True flag means #that rospy will choose a unique name for our 'listener' node 
#so that multiple listeners can run simultaneously.
        rospy.init_node('listener', anonymous=True)
	rospy.Subscriber("audio", String, callback)
# spin() simply keeps python from exiting until this node is stopped
       rospy.spin()
      
# Start main function 
if  __name__ == '__main__':
         listener()
         t2s()




