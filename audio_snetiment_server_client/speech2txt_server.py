#!/usr/bin/env python
#Copyright 2018 Sarath K

#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from audio_to_sentiment1.srv import *
import rospy
#handle function definition
def handle_analysis(req):
#creating neutral, positive and negative word list
    neutral=['hello','hai','day','time']
    positive=['smile','happy','beautiful','good']
    negative=['no','bad','cry','sorrow']
#getting data from client
    z=req.a
    print "converted text is ",req.a
    print "Returning"
#performing sentimental analysis of converted text
    for i in neutral:
      if i==z:
        m='The text is neutral'
    
    for i in positive:
      if i==z:
        m='The text is positive'
    for i in negative:
      if i==z:
        m='The text is negative'
#returning the result back to client
    return SentimentResponse(m)

def speech_server():
#creating server node
    rospy.init_node('speech_server')
    s = rospy.Service('analysis', Sentiment, handle_analysis)
    print "Analysis is ready"
    rospy.spin()
#start of main function
if __name__ == "__main__":
    speech_server()

